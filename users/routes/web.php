<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

$router->options('{all:.*}', ['middleware' => 'cors', function() {
    return response('', 200);
}]);

$router->get('/api/user/authorize', 'UserController@authorizeToken');

$router->group(['prefix' => 'api'], function ($router) {
    $router->group(['middleware' => ['cors']], function($router){
        $router->post('/user/register', 'UserController@register');
        $router->post('/user/login', 'UserController@login');
        $router->post('/user/authorize', 'UserController@authorizeToken');
    });
});