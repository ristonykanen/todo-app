<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register( Request $request ){

        $validationRules = [
            'name' => 'required|unique:users',
            'password' => 'required',
            'passwordAgain' => 'required'
        ];

        $validationMessages = [
            'name.required' => 'Missing a required field',
            'unique' => 'Name already in use'
        ];

        $validator = Validator::make( $request->all(), $validationRules, $validationMessages );

        if ( $validator->fails() ) {
            return response()->json([
                'success' => false, 
                'message' => 'Registering new account failed'
            ]);
        }

        $credentials = $request->only(['name', 'password', 'passwordAgain']);

        if( $credentials['password'] != $credentials['passwordAgain'] )
            return response()->json(['success' => false, 'message' => 'passwords doesn\'t match'], 400);

        $user = new User();
        $user->name = $credentials['name'];
        $user->password = $credentials['password'];
        
        if($user->save()){
            return response()->json(['success' => true, 'message' => 'New user registered']);
        } else {
            return response()->json(['success' => false, 'message' => 'Registering new account failed']);
        }

    }

    public function login(Request $request)
    {

        $credentials = $request->only(['name', 'password']);

        try {
            $token = JWTAuth::attempt($credentials);
            if (!$token) {
                return response()->json(['success' => false, 'message' => 'Login failed. Wrong credentials'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['success' => false, 'message' => 'token_expired'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['success' => false, 'message' => 'token_invalid'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
        
        $user = \Auth::user();
        
        $response = [
            'success' => true,
            'token' => $token, 
            'user' => [ 'name' => $user->name ]
        ];

        return response()->json($response);

    }

    public function authorizeToken(Request $request){
        $payload = \Auth::payload()->toArray();
        $data = [
            'id' => $payload['id'], 
            'name' => $payload['name']
        ];
        return response()->json(['success' => true, 'data' => $data]);
    }

    
}
