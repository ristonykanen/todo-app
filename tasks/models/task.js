'use strict';
module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    due_to: DataTypes.DATE,
    finished: DataTypes.DATE
  }, {tableName:'tasks'});
  Task.associate = function(models) {
    // associations can be defined here
  };
  return Task;
};