const express = require("express");
const Sequelize = require("sequelize"); 
const bodyParser = require("body-parser");
const app = express();
const requestPromise = require('request-promise');
const Task = require('./models').Task;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

const sequelize = new Sequelize('todo', 'root', 'p4ssw0rd_', {
    host: 'db',
    dialect: 'mysql'
})

sequelize.authenticate()
.then(() => {
    console.log('Connection has been established successfully.');
})

const asyncHandler = fn => (req, res, next) =>
  Promise
    .resolve(fn(req, res, next))
    .catch(next)

/* middleware to check and parse jwt */
app.use(asyncHandler( (req, res, next) => {
    const token = req.headers['authorization'];

    var options = {
        method: 'POST',
        headers: { "Content-Type": "application/json", "Authorization": token },
        json: true,
        simple: false,
        uri: 'http://users/api/user/authorize',
    };

    requestPromise(options)
    .then(function (response) {
        if(response.success){
           req.body.user_id = response.data.id
           req.body.username = response.data.name
           next();
        }
    })
    .catch(function (err) {
        next(err);
    });
    
})); 


app.get("/", (req, res) => {
    res.json({msg: "tasks"});
});

app.post("/api/tasks", (req, res) => {
    Task.findAll(
        { where:{ user_id: req.body.user_id } }
    )
    .then((tasks) => {
        res.json({success:true, tasks: tasks});
    })
});

app.post("/api/tasks/finished", (req, res) => {
    Task.findAll(
        {   
            where:{ 
                user_id: req.body.user_id,
                finished: { $not: null } 
            }
        }
    )
    .then((tasks) => {
        res.json({success:true, tasks: tasks});
    })
});

app.post("/api/task/create",(req, res) => {
    
    Task.create({
        name: req.body.name,
        description: req.body.description,
        due_to: req.body.dueTo,
        user_id: req.body.user_id
    })
    .then((task) => {
        res.json({success: true, message: 'Task created successfully', task: task });
    })
    .catch((err) => {
        res.json({success: false, message: 'Failed to create task'});
    })
    
});

app.post("/api/task/finish", (req, res) => {
    Task.findOne({ where: {user_id: req.body.user_id, id: req.body.taskId } })
    .then((task) => {
        task.update({ finished: new Date().toISOString() })
        .then((task) => {
            res.json({success: true, task: task});
        })
        .catch((err) => {
            res.json({success: false, message: 'Updating task failed'});
        })
    })
    .catch((err) => {
        res.json({success: false, message: 'Updating task failed'});
    });
});

app.post("/api/task/delete", (req, res) => {
    Task.findOne({ where: {user_id: req.body.user_id, id: req.body.taskId } })
    .then((task) => {
        task.destroy();
        res.json({success: true, message: 'Task deleted'});
    }).catch((err) => {
        res.json({success: false, message: 'Failed to delete task'});
    })
});

app.listen( 3000, () => {
    console.log('Server running on port 3000');
});

