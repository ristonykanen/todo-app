import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import { store } from './store/store';

axios.defaults.baseURL = 'http://127.0.0.1:8080/api/';

axios.defaults.headers.post["Content-Type"] = "application/json";
if(localStorage.getItem('todojwt')){
  axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('todojwt')}`;
}

Vue.use(VueAxios, axios);

axios.interceptors.response.use((response) => {

  if(response.data.success != undefined && response.data.message != undefined){
    store.commit('user/message', {success: response.data.success, content: response.data.message});
  }
  
  return response;
}, function (error) {
  if ( error.response != "undefined" && error.response.status === 401) {
    localStorage.setItem('todojwt', null);
    store.commit('user/jwt', null);
    store.commit('navigation/currentPage', 'login');
  }

  if(error.response.data.success != undefined && error.response.data.message != undefined){
    store.commit('user/message', {success: error.response.data.success, content: error.response.data.message});
  }

  return Promise.reject(error.response);
});


Vue.filter('truncate', (text, length, clamp) => {
  clamp = clamp || '';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
});

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})