import Vue from 'vue'
import Vuex from 'vuex'

import user from './modules/user'
import navigation from './modules/navigation'
import todo from './modules/todo'

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        user,
        navigation,
        todo
    }
});