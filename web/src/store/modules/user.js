import axios from 'axios'

const getDefaultState = () => {
    return {
        name: null,
        jwt: null,
        messages: []
    }
}

const state = getDefaultState()

const getters = {
    name(state){
        return state.name;
    },
    jwt(state){
        return state.jwt;
    },
    message(state){
        return state.messages[0];
    }
}

const mutations = {
    name(state, name){
        state.name = name;
    },
    jwt(state, jwt){
        state.jwt = jwt;
    },
    message(state, message){
        state.messages = message && message != '' ? [message] : [];
    },
    removeMessage(state){
        if(state.messages.length > 0){
            state.messages.splice(0, 1)
        }
    },
    logout(state){
        Object.assign(state, getDefaultState());
        localStorage.setItem('todojwt', null);
        window.location.reload(false); 
    }
}

const actions = {
    async LOGIN(context, credentials){
        await axios.post('user/login', credentials).then( (response) => {
            context.commit('name', response.data.user.name);
            context.commit('jwt', response.data.token);
            context.commit('navigation/currentPage', 'todo', { root: true });
            localStorage.setItem('todojwt', response.data.token);
            axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token}`;
        }).catch( (err) => {
        });

    },
    async REGISTER(context, credentials, rootState){
        return await axios.post('user/register', credentials)
        .then( (response) => {
            if(response.data.success){
                rootState.commit('navigation/currentPage', 'login');
            }
        }).catch( (err) => {
        });
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}