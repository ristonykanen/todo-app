const getDefaultState = () => {
    return {
        currentPage: 'login'
    }
}

const state = getDefaultState();

const getters = {
    currentPage(state){
        return state.currentPage;
    }
}

const mutations = {
    currentPage(state, pageName){
        state.currentPage = pageName;
    },
    resetState(state){
        Object.assign(state, getDefaultState())
    }
}

const actions = {
    
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}