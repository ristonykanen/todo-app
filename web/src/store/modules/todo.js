import axios from 'axios'

const getDefaultState = () => {
    return {
        tasks: [],
        selectedTask: null
    }
}

const state = getDefaultState();

const getters = {
    tasks(state){
        return state.tasks;
    },
    todo(state){
        return state.tasks.filter( tasks => tasks.finished == undefined || tasks.finished == null );
    },
    done(state){
        return state.tasks.filter( task => task.finished && task.finished != null);
    },
    selectedTask(state){
        return state.selectedTask;
    }
}

const mutations = {
    addTask(state, task){
        state.tasks.push(task);
    },
    removeTask(state, taskId){
        let objIndex = state.tasks.findIndex((t => t.id == taskId));
        if(objIndex){
            state.tasks.splice(objIndex, 1);
        }
    },
    setTasks(state, tasks){
        state.tasks = tasks;
    },
    selectedTask(state, taskId){
        state.selectedTask = state.tasks.find( task => task.id == taskId);
    },
    finishTask(state, task){
        let objIndex = state.tasks.findIndex((t => t.id == task.id));
        if(objIndex){
            state.tasks[objIndex].finished = task.finished;
            state.selectedTask = null;
        }
            
    },
    resetState(state){
        Object.assign(state, getDefaultState());
    }
}

const actions = {
    async GET_TASKS(context, view){
        let route = ''
        if(view == 'todo'){
            route = 'tasks';
        } else {
            route = 'tasks/finished';
        }

        let response = await axios.post(route)
        if(response.data.success){
            context.commit('setTasks', response.data.tasks)
        }

    },
    async DELETE_TASK(context, taskId){
        let response = await axios.post('task/delete', {taskId: taskId})
        if(response.data.success){
            context.commit('removeTask', taskId)
        }

    },
    async FINISH(context, taskId){
        let response = await axios.post('task/finish', {taskId: taskId})
        if(response.data.success){
            context.commit('finishTask', response.data.task);
        }
    },
    async SAVE(context, taskData){
        return await axios.post('task/create', taskData)
        .then((response) => {
            if(response.data.success){
                context.commit('addTask', response.data.task);
            }
        })
        .catch((err) => {});
    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}