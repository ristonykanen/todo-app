const webpack = require('webpack');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const config = {
  entry: './src/app.js',
  mode: 'development',
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'src': path.join(__dirname, 'src/')
    }
  },
  output: {
    //path: path.resolve(__dirname, 'dist/'),
    //filename: 'bundle.js'
    filename: './dist/bundle.js'
  },
  devServer: {
    host: '0.0.0.0',
    compress: true,
    port: 3000
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          plugins:[ 'transform-object-rest-spread' ]
        }
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.png$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              mimetype: 'image/png'
            }
          }
        ]
      }
    ]
  },
  watchOptions: {
    poll: true,
    aggregateTimeout: 300,
    poll: 1000
  },
  watch: true,
  plugins: [
    new VueLoaderPlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/)
  ]
}

module.exports = config;